package com.animana.Fixtures;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.animana.utils.Log;


public class LoginFixture {
	private final WebDriver webDriver;
			
	public LoginFixture(WebDriver webDriver) {
	    this.webDriver = webDriver;
	  } 
	
	/**
	 * This is a sample test only for USER STORY : MANA-8100
	 * 
	 */
	public boolean sampleLoginTest(){
		Log.info("Sample Test for MANA-8100 Starts");
		//Enter User Credentials
		webDriver.findElement(By.id("username")).sendKeys("e2e0");
		webDriver.findElement(By.id("password")).sendKeys("123@YesWeCan");
		
		//Click Login button
		webDriver.findElement(By.cssSelector("input.button.expand")).click();
		Log.info("Sample Test Completes");
		return true;		
	}
		
	/**
	 * Close the Browser after completion of each test
	 * This part is taken care at Tear Down of each Test Suite level
	 * */
	public void stopBrowser()
	{
		Log.info("Closing the browser");
		webDriver.quit();
	}
		
}
