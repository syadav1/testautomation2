package com.animana.Fixtures;

import org.openqa.selenium.WebDriver;

import com.animana.CustomExceptions.UrlException;
import com.animana.utils.Log;


public class SeleniumDriverFixture {
	
		public SeleniumDriverFixture() {
			super();
		}
		
		/**
		 *  <p><code>
     		* | start driver | <i>$Driver</i> | on url | <i>http://localhost</i> |
     		* </code></p>
		 * @param driver : a WebDriver instance
		 * @param url : URL of the application 
		 * */
		public void startDriverOnUrl(final WebDriver driver, String url) throws Exception{
			try{
			url = removeAnchorTag(url);
			Log.info("Tested Url :"+url);
			driver.get(url);
			}
			catch(Exception e)
			{
				Log.error("Invalid URL", e);
				throw new UrlException("Invalid URL "+url);
			}
		}
		
		/**
		 * scriptName is something like
		 * '<a href="https://dev.animana.com">https://dev.animana.com</a>'.
		 * 
		 * @param scriptName
		 * @return
		 */
		public static String removeAnchorTag(String scriptName) {
			if (scriptName.startsWith("<a") && scriptName.endsWith("</a>")) {
				scriptName = scriptName.split(">", 2)[1].split("<", 2)[0];
			}
			return scriptName;
		}
		
		/**
		 * <p><code>
			 * | start mobiledriver | <i>$Driver</i> | on url | <i>http://localhost</i> |
			 * </code></p>
		 *
		 * @param driver : a WebDriver instance
		 * @param url : URL of the application
		 */
		public void startMobiledriverOnUrl(final WebDriver driver,String url) throws Exception{
			try{
			url = removeAnchorTag(url);
			Log.info("Tested URL on Mobile :"+url);
			driver.get(url);
			}
			catch(Exception e)
			{
				Log.error("Invalid URL", e);
				throw new UrlException("Invalid URL "+url);
			}
		}
		
}
