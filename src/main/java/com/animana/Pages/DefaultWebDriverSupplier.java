package com.animana.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.animana.CustomExceptions.CustomException;
import com.animana.CustomExceptions.IllegalBrowserException;
import com.animana.utils.Log;

public class DefaultWebDriverSupplier {
	
	public WebDriver driver;
	private String browser;
	private String driverpath;
	
	/**
	 * Setting the Expected browser required for test
	 * @param browser
	 * browser is provided at Test_Config of Wiki Page of Fitnesse
	 */
	public void setBrowser(String browser) {
		this.browser = browser;
	}
  
	/**
	 * Setting the Expected DriverPath having the path details for the Browser Driver binaries on local system
	 * @param driverPath
	 * driverPath is provided at Test_Config of Wiki Page of Fitnesse
	 */
	public void setPath(String driverpath) {
		this.driverpath = driverpath;
	}
	
	/**
	 * <p><code>
		 * |$WebDriver| new web driver |
		 * </code></p>
	 * This method returns an instance of WebDriver depending up on the type of test environment
	 * @return an instance of WebDriver
	 * @throws CustomException
	 */
	public WebDriver newWebDriver() throws Exception {
		
		try{
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", driverpath);
				driver = new ChromeDriver();
				
			}
			else if(browser.equalsIgnoreCase("iexplore")){
				System.setProperty("webdriver.ie.driver", driverpath);
				driver = new InternetExplorerDriver();
			
			}
			else if(browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", driverpath);
				driver = new FirefoxDriver();
				
			}
			else if(browser.contains("remote")){
				try{
				driver = new RemoteWebDriverSupplier(browser).get();
				}
				catch(IllegalBrowserException e){
					Log.error("Illegal browser type for Remote Session!!", e);
					throw new IllegalBrowserException("Unknown or Missing browser type. Should be one of 'firefox', 'iexplore', 'chrome', 'safari'");
				}
			}
			else {
				throw new IllegalBrowserException("Unknown or Missing browser type. Should be one of 'firefox', 'iexplore', 'chrome' to run on Local system!!");
			}
			
		}catch(IllegalBrowserException e)
		{
			Log.error("Illegal browser type!!", e);
			
		}
		return driver;
	}
}
