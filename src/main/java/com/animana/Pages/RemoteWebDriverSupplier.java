package com.animana.Pages;

import static org.openqa.selenium.remote.CapabilityType.PLATFORM;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.animana.CustomExceptions.CustomException;
import com.animana.CustomExceptions.IllegalBrowserException;
import com.animana.CustomExceptions.UrlException;
import com.animana.utils.Log;

public class RemoteWebDriverSupplier {
	
		private static final String REMOTE = "remote";
		private String remote;
		private Map<String, String> capabilities;
		
		/**
		 *  This method takes a String input and change it a jsonObject.
		 * Read the value for the key "remote" and remaining json data is provided to Desired Capabilities
		 * @throws CustomException 
		 * */
		public RemoteWebDriverSupplier(String json) throws Exception {
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(json);
			} catch (JSONException e) {
				Log.error("Unable to interpret browser information", e);
				throw new IllegalBrowserException("Unable to interpret browser information", e);
			}

			try {
				remote = jsonObject.getString(REMOTE);
				jsonObject.remove(REMOTE);
				capabilities = jsonObjectToMap(jsonObject);
			} catch (JSONException e) {
				Log.error("Provided browser  is a Illigal browser type!!", e);
				throw new IllegalBrowserException("Unable to fetch required fields from json string", e);
			}
		}
		
		/**
		 *  This method convert the json object and returns a hash map instance
		 *  */
		private Map<String, String> jsonObjectToMap(JSONObject jsonObject) throws JSONException{
			// Assume you have a Map<String, String> in JSONObject
			@SuppressWarnings("unchecked")
			Iterator<String> nameItr = jsonObject.keys();
			Map<String, String> outMap = new HashMap<String, String>();
			while(nameItr.hasNext()) {
				String name = nameItr.next();
			    outMap.put(name, jsonObject.getString(name));
			}

		    String platform = outMap.get(PLATFORM);
		    if (platform != null) {
		    	outMap.put(PLATFORM, platform.toUpperCase());
		    }

			return  outMap;
		}
		
		/** Return URL required to start the sauce environment
		 * */
		public URL getSauceRemote() throws IOException {
			try {
				
				return new URL(remote);
			} catch (MalformedURLException e) {
				Log.error("URL is not a valid URL", e);
				throw new UrlException("URL is not a valid URL");
			}
		}
		
		/* Returns instance of Capabilities, required to instantiate RemoteWebDriver*/
		public Capabilities getCapabilities() throws CustomException {
			try{
				return new DesiredCapabilities(capabilities);
			}
			catch(SessionNotCreatedException e)
			{
				Log.error("Invalid Capabilities provided", e);
				throw new CustomException("Invalid Capabilities provided");
			}
		}
		
		/* Returns an instance of WebDriver */
		public WebDriver get() throws Exception {
			return new Augmenter().augment(new RemoteWebDriver(getSauceRemote(), getCapabilities()));
		}

		
}
