package com.animana.CustomExceptions;

@SuppressWarnings("serial")
public class IllegalBrowserException extends RuntimeException{

	public IllegalBrowserException(String message) {
		super(message);
	}

	public IllegalBrowserException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
