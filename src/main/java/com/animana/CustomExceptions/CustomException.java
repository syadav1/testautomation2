package com.animana.CustomExceptions;

import com.animana.utils.Log;

@SuppressWarnings("serial")
public class CustomException extends Exception{
	
	public CustomException(String msg)
	{
		super(msg);
		Log.info(msg);
	}

}


