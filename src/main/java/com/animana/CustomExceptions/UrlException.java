package com.animana.CustomExceptions;

@SuppressWarnings("serial")
public class UrlException extends RuntimeException{
	
	public UrlException(String msg){
		super(msg);
	}
	
	public UrlException(String message, Throwable cause){
		super(message,cause);
	}
}
