!include -c Test_Config

!**> startscenarios

|scenario|startsauce|
|set browser| { "name": "SaucelabTest", "remote": "!-http://-!${sauce.user}:${sauce.accesskey}@ondemand.saucelabs.com:80/wd/hub", "browserName": "${S_BROWSER}", "platform": "${S_PLATFORM}" } |
| $webDriver= | new web driver |
| start driver |$webDriver| on url |  ${url} |


|scenario|startdev|
|set browser| { "name": "SauceDevtest", "remote": "!-http://-!${sauce.user}:${sauce.accesskey}@ondemand.saucelabs.com:80/wd/hub", "browserName": "${BROWSER}", "platform": "Windows 7" } |
| $webDriver= | new web driver |
| start driver |$webDriver| on url |  ${url} |


|scenario|startdevlocal|
|set browser|${browser}|
|set path|${driverpath}|
| $webDriver=| new web driver |
| start driver |$webDriver| on url |  ${url} ||


|scenario|startsaucemobile|
| $webDriver=|set remote mobile browser| { "name": "Xebiumtest", "remote": "!-http://-!${sauce.user}:${sauce.accesskey}@ondemand.saucelabs.com:80/wd/hub","appiumVersion": "1.5.3","deviceName":"${S_DEVICE}", "deviceOrientation":"portrait","browserName":"${S_BROWSER}","platformVersion":"${S_VERSION}","platformName":"${S_PLATFORM}"}|
| start mobiledriver |$webDriver| on url | https://dev.animana.com |


****************!

