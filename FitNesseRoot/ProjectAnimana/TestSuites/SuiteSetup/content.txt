!contents -R2 -g -p -f -h

!|import             |
|com.animana.Fixtures|
|com.animana.utils   |
|com.animana.Pages   |

|library                    |
|selenium driver fixture    |
|default web driver supplier|
|appium web driver supplier |
#|remote web driver supplier |

|script     |
|${startenv}|


